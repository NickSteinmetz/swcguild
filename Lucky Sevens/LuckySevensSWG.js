function hideResults(){
	document.getElementById("results").style.display = "none";
}
/* Declaring functions */
function play(){
	var startingBet = document.getElementById("betInput").value;
	var bet = startingBet;
	var diceOne = Math.floor((Math.random() * 6) + 1);
	var diceTwo = Math.floor((Math.random() * 6) + 1);
	var diceRoll = diceOne + diceTwo;
	var betsArray = new Array();
	
	
	/* Game Logic */
	while (bet > 0){
		if(diceRoll != 7) {
			bet -= 1	
		}	else {
			bet += 4
		}
		betsArray.push(bet);
		var diceOne = Math.floor((Math.random() * 6) + 1);
		var diceTwo = Math.floor((Math.random() * 6) + 1);
		var diceRoll = diceOne + diceTwo;
	}
	
	/* Variables for the Table */
	
	var rollCounter = betsArray.length;
	var highestAmount = Math.max.apply(Math, betsArray);
	var highestRolls = betsArray.indexOf(highestAmount);
	var richestRoll = rollCounter - highestRolls;
	
	/* Populating the HTML Table */
	
	function showResults() {
		document.getElementById("results").style.display = "inline";
		document.getElementById("playButton").innerHTML = "Play Again";
		document.getElementById("resultsStartBet").innerHTML = "$" + startingBet + ".00";
		document.getElementById("resultsRollCounter").innerHTML = rollCounter;
		document.getElementById("resultsHighestAmount").innerHTML = "$" + highestAmount + ".00";
		document.getElementById("resultsRichestRoll").innerHTML = richestRoll;
	};
		
		showResults();
		
	}
	
	

	